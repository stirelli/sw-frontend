export interface IAuthor {
  id: string;
  firstName: string;
  lastName: string;
  email: string;
  dateOfBirth: string;
  fullName: string;
}

export class Author implements IAuthor {
  id: string;
  firstName: string;
  lastName: string;
  email: string;
  dateOfBirth: string;

  constructor(author: IAuthor) {
    this.id = author.id;
    this.firstName = author.firstName;
    this.lastName = author.lastName;
    this.email = author.email;
    this.dateOfBirth = author.dateOfBirth;
  }

  public get fullName(): string {
    return `${ this.firstName } ${ this.lastName }`;
  }
}
