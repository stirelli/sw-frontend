export interface IPublication {
  id: string;
  title: string;
  body: string;
  datetime: string;
  authorId: string;
  authorName: string;
}

export class Publication implements IPublication {
  id: string;
  title: string;
  body: string;
  datetime: string;
  authorId: string;
  authorName: string;

  constructor(author: IPublication) {
    this.id = author.id;
    this.title = author.title;
    this.body = author.body;
    this.datetime = author.datetime;
    this.authorId = author.authorId;
    this.authorName = author.authorName;
  }
}
