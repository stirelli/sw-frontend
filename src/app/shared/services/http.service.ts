import { Injectable } from '@angular/core';
import { CRUDOperation, DataType } from '../constants/global.constants';
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HttpService {
  private readonly baseURL = environment.webServices.base;

  constructor(private httpClient: HttpClient) { }

  private getEndpointURL(dataType: DataType, operation: CRUDOperation): string {
    const endpoint = environment.webServices.endpoints[dataType][operation];
    return this.baseURL + endpoint;
  }

  public get(dataType: DataType): Observable<any> {
    const url = this.getEndpointURL(dataType, CRUDOperation.Read);
    return this.httpClient.get(url);
  }
}
