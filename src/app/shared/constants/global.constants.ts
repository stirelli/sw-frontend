export enum RoutePath {
  Index = '',
  Publications = 'publications',
  Authors = 'authors'
}

export enum CRUDOperation {
  Create = 'create',
  Read = 'read',
  Update = 'update',
  Delete = 'delete'
}

export enum DataType {
  Authors = 'authors',
  Publications = 'publications'
}
