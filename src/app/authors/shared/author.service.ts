import { Injectable } from '@angular/core';
import { HttpService } from '../../shared/services/http.service';
import { Author, IAuthor } from '../../shared/models/authors.model';
import { Observable, of } from 'rxjs';
import { CRUDOperation, DataType } from '../../shared/constants/global.constants';
import { map, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthorService {
  private authors: IAuthor[] = [];

  constructor(private httpService: HttpService) { }

  private loadAuthors(): Observable<IAuthor[]> {
    return this.httpService.get(DataType.Authors).pipe(
      map((response: any) => {
        response.data.map((rawAuthor: IAuthor) => {
          this.authors.push(new Author(rawAuthor));
        });
        this.authors.sort(this.orderArray);
        return this.authors;
      })
    );
  }

  public getAuthors(): Observable<IAuthor[]> {
    if (this.authors.length) {
      return of(this.authors);
    }

    return this.loadAuthors();
  }

  private orderArray(a, b) {
    if (a.firstName > b.firstName) {
      return 1;
    }
    if (a.firstName < b.firstName) {
      return -1;
    }
    return 0;
  }
}
