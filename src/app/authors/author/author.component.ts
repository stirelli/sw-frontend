import { Component, Input, OnInit } from '@angular/core';
import { IAuthor } from '../../shared/models/authors.model';
import { PublicationService } from '../../publications/shared/publication.service';

@Component({
  selector: 'app-author',
  templateUrl: './author.component.html',
  styleUrls: ['./author.component.scss']
})
export class AuthorComponent implements OnInit {
  @Input('author') author: IAuthor;

  constructor(private publicationService: PublicationService) { }

  ngOnInit() {
  }

  public filterByAuthor(authorId: string) {
    this.publicationService.sendFilterAuthor(authorId);
  }
}
