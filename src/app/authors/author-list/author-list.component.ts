import { Component, OnInit } from '@angular/core';
import { AuthorService } from '../shared/author.service';
import { IAuthor } from '../../shared/models/authors.model';
import { Observable, PartialObserver, Subscription } from 'rxjs';

@Component({
  selector: 'app-author-list',
  templateUrl: './author-list.component.html',
  styleUrls: ['./author-list.component.scss']
})
export class AuthorListComponent implements OnInit {
  public authors: Observable<IAuthor[]>;

  constructor(private authorService: AuthorService) { }

  ngOnInit() {
    this.getAuthors();
  }

  private getAuthors(): void {
    this.authors = this.authorService.getAuthors();
  }
}
