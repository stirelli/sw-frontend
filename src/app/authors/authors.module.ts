import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthorsRoutingModule } from './authors-routing.module';
import { AuthorComponent } from './author/author.component';
import { AuthorListComponent } from './author-list/author-list.component';
import { AuthorsMaterialModule } from './authors-material.module';

@NgModule({
  declarations: [AuthorComponent, AuthorListComponent],
  exports: [
    AuthorComponent,
    AuthorListComponent
  ],
  imports: [
    CommonModule,
    AuthorsRoutingModule,
    AuthorsMaterialModule
  ]
})
export class AuthorsModule { }
