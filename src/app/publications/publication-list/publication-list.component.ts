import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { IPublication, Publication } from '../../shared/models/publications.model';
import { PublicationService } from '../shared/publication.service';
import { filter } from 'rxjs/operators';
import { Subscription } from 'rxjs';

interface IDataSourceFilter {
  value: string;
  key: string;
}

@Component({
  selector: 'app-publication-list',
  templateUrl: './publication-list.component.html',
  styleUrls: ['./publication-list.component.scss']
})
export class PublicationListComponent implements OnInit, OnDestroy {
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;

  public displayedColumns = ['title', 'body', 'authorName', 'datetime'];
  public dataSource: MatTableDataSource<Publication>;
  private filterObj: IDataSourceFilter;
  private filterAuthorSubscription: Subscription;
  private publicationSubscription: Subscription;

  constructor(private publicationService: PublicationService) { }

  ngOnInit() {
    this.getPublications();
  }

  private getPublications(): void {
    this.publicationSubscription = this.publicationService.getPublications().subscribe((publications) => {
      this.initTable(publications);
    }, (error => {
      // handle the error
      console.log(error);
    }));
    this.filterAuthorSubscription = this.publicationService.getFilterAuthorSubject().subscribe(
      (authorId: string) => {
        this.applyFilter(authorId, 'authorId');
      }, (error => {
        // handle the error
        console.log(error);
      })
    );
  }

  private initTable(publications: IPublication[]) {
    this.dataSource = new MatTableDataSource<Publication>(publications);
    this.dataSource.filterPredicate = (data) => {
      if (data[this.filterObj.key] && this.filterObj.key) {
        return data[this.filterObj.key].toLowerCase().includes(this.filterObj.value);
      }
      return false;
    };
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  public applyFilter(filterValue: string, filterKey: string) {
    this.filterObj = {
      value: filterValue.trim().toLowerCase(),
      key: filterKey
    };
    this.dataSource.filter = filterValue;
  }

  ngOnDestroy() {
    // unsubscribe to ensure no memory leaks
    this.publicationSubscription.unsubscribe();
    this.filterAuthorSubscription.unsubscribe();
  }
}
