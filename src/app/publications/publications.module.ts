import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PublicationsRoutingModule } from './publications-routing.module';
import { PublicationListComponent } from './publication-list/publication-list.component';
import { PublicationsMaterialModule } from './publications-material.module';
import { AuthorsModule } from '../authors/authors.module';

@NgModule({
  declarations: [
    PublicationListComponent
  ],
  imports: [
    CommonModule,
    PublicationsRoutingModule,
    PublicationsMaterialModule,
    AuthorsModule
  ]
})
export class PublicationsModule {}
