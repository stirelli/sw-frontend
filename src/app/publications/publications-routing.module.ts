import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RoutePath } from '../shared/constants/global.constants';
import { PublicationListComponent } from './publication-list/publication-list.component';

const routes: Routes = [
  {
    path: RoutePath.Index,
    component: PublicationListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PublicationsRoutingModule { }
