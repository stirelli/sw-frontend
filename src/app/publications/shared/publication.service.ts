import { Injectable } from '@angular/core';
import { IPublication, Publication } from '../../shared/models/publications.model';
import { CRUDOperation, DataType } from '../../shared/constants/global.constants';
import { HttpService } from '../../shared/services/http.service';
import { map, tap } from 'rxjs/operators';
import { Observable, of, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PublicationService {
  private publications: IPublication[] = [];
  private filterAuthorSubject = new Subject<string>();

  constructor(private httpService: HttpService) { }

  private loadPublications(): Observable<IPublication[]> {
    return this.httpService.get(DataType.Publications).pipe(
      map((response: any) => {
        return response.data.map((rawPublication: IPublication) => {
          return new Publication(rawPublication);
        });
      }),
      tap((publications: IPublication[]) => {
        this.publications = publications;
      })
    );
  }

  public getPublications(): Observable<IPublication[]> {
    if (this.publications.length) {
      return of(this.publications);
    }

    return this.loadPublications();
  }

  public sendFilterAuthor(authorId: string) {
    this.filterAuthorSubject.next(authorId);
  }

  public clearFilterAuthor() {
    this.filterAuthorSubject.next();
  }

  public getFilterAuthorSubject() {
    return this.filterAuthorSubject.asObservable();
  }
}
