export const environment = {
  production: true,
  webServices: {
    base: 'https://9q5u51q3w7.execute-api.us-east-1.amazonaws.com/dev/api/',
    endpoints: {
      publications: {
        create: 'publications/new',
        read: 'publications',
        update: 'publications/edit',
        delete: 'publications/remove'
      },
      authors: {
        create: 'authors/new',
        read: 'authors',
        update: 'authors/edit',
        delete: 'authors/remove'
      }
    }
  }
};
