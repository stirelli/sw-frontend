# Sweatworks Frontend Challenge

## Challenge

Sr. FULL STACK

Your task is to make a project using Angular 4+ that uses a REST API based on NodeJS using Serverless and DynamoDB.

The site has to handle a database of authors and their publications.

Each publication needs to have the date and time, body and title.

Each author needs to have their name, email and date of birth.

You are asked to create an API to perform CRUD (Create, Read, Update and Delete) operations.

Each endpoint and its possible errors should be tested using https://github.com/visionmedia/supertest.

This project must be prepared to be deployed to different environments without modifying the code.

The front-end needs to show the publication list ordered by date, as well as the name of the author <name, email>.

Use SASS and follow the component development approach for your frontend ( required )

Use BEM and adapt your layout to be responsive with media queries ( desirable )

The user should be able to invert the order of the publications showing the oldest ones first.

All the authors should be listed in a sidebar, and the user should be able to click on one and see a list of all of his/her publications.

The user should also be able to search publications by title (this has to be paginated).

Create a public repository (github or other service) and upload the code. Do not forget to include a clear documentation that allows us to start and test the project locally. Anything that is not specified here is up to your criteria, taking into account all the points mentioned above.

# Getting started

*Requires Node 10 or higher*

Clone this repo, and cd into it:
```
git clone https://stirelli@bitbucket.org/stirelli/sw-frontend.git
cd sw-frontend
```

## Starting the local server with mocked data

```
npm install
npm run start-mock
```

## Starting the local server with local api and DynamoDB downloadable version at port 8000 

```
npm install
npm run start-dev
```

## Starting the local server with deployed api and DynamoDB in AWS 

```
npm install
npm run start-prod
```

## Deploying to aws 

```
npm install
npm run build-prod
npm run aws-deploy
```

## 
I already uploaded the frontend to: 
http://sw-frontend-challenge-app.s3-website-us-east-1.amazonaws.com/
